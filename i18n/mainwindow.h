﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void reTranslate(const QString& lang);

private slots:
    void on_actionEnglish_triggered();
    void on_actionChinese_triggered();
    void on_actionJapanese_triggered();

private:
    Ui::MainWindow *ui;
    QTranslator* translator;
};

#endif // MAINWINDOW_H
